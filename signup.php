<?php 
 require_once 'config.php';
 session_start();
?>
<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device.width">
    <title>Green-Help | Welcome</title>
    <link rel="stylesheet" href="./styles/home.css">
</head>

<body>
<?php
        require_once 'header.php';
        ?>

<?php

        $Uname = $_REQUEST["uname"];
        $Upass = $_REQUEST["pw"];
        $Udevice = $_REQUEST["device"];
        $Uplant = $_REQUEST["plant"];
        $Umail = $_REQUEST["mail"];

        $sql1 = " SELECT * FROM eco_user where uname_eco = '$Uname'";
        $result = mysqli_query( $conn , $sql1);
        if (mysqli_num_rows($result)> 0) { ?>
         <br> <br>
                   <section id='show'>
                   
            <div class='box'>
                <div class='inner-box'>
                    <h3> Entered user name is already used!</h3>
                    <p style='text-align: center;'> Please retry with a unique user name!</p> <br>
                    <p style='text-align: center;'>
                        <a class='link' href='http://localhost/greenhelp/login.php'>back to signup</a>
    
    
                    </p>
                </div>
            </div>
        </section> 
        <?php }
        else{


            $sql = "INSERT INTO eco_user (uname_eco , UID_eco , mail_eco ,tele_eco , empname_eco , pw_eco ,position_eco)
            VALUES ('$Uname', '$empID' , '$UEmail' , '$Utele' ,'$UFName', '$Upass' , '$position' )";
            
    
            if ($conn->query($sql) === TRUE) { ?>
     <br> <br>
                <section id='show'>

                <div class='box'>
                    <div class='inner-box'>
                        <h3> Thank you for creating an account!</h3>
                        <p style='text-align: center;'> Please log-in to the system!</p><br>
                        <p style='text-align: center;'>
                            <a class='link' href='http://localhost/greenhelp/login.php'>Go to login page</a>
        
        
                        </p>
                    </div>
                </div>
            </section> 
            <?php
              } else {
                ?>
                 <br> <br>
                <section id='show'>
                <div class='box'>
                    <div class='inner-box'>
                        <h3> Oops! something went wrong! Please try again!</h3><br>
                        <p style='text-align: center;'>
                            <a class='link' href='http://localhost/greenhelp/login.php'>back to sign up page</a>
        
        
                        </p>
                    </div>
                </div>
            </section> 
            <?php
              }
            }
    
            ?>


<?php
        require_once 'footer.php';
        ?>
    </body>

    </html>