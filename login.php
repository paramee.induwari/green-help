<?php 
 require_once 'config.php';
 session_start();
?>
<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device.width">
    <meta name="description" content="aquatic plants |  IoT">
    <title>Green Help | Welcome</title>
    <link rel="stylesheet" href="./styles/login.css">
    <link rel="stylesheet" href="./styles/header.css">
</head>

<body>
<?php
        require_once 'header.php';
        ?>
  <br>
  <section id="log">
        <div class="container" id="container">
	<div class="form-container sign-up-container">
		<form action="signup.php" type="post" enctype="multipart/form-data">
			<h1>Create Account</h1>
			
			<input type="text" placeholder="User name" name="uname" required />
            <input type="text" placeholder="Device ID" name="device" required />
            <input type="text" placeholder="Plant Name" name="plant" required />
			<input type="password" placeholder="Password" name="pw" required />
             <br>
			<input type="submit"  class="button" value="Sign Up" >
		</form>
	</div>
	<div class="form-container sign-in-container">
		<form action="account.php" type="post" enctype="multipart/form-data" >
			<h1>Login</h1>
			<br>
			
			<input type="text" placeholder="User Name" name="uname" required />
			<input type="password" placeholder="Password" name="pw" required />
			<br>
			<input type="submit"  class="button" value="Log In" >

			<a href="resetpw.php"> Forgot password? </a>
		</form>
	</div>
	<div class="overlay-container">
		<div class="overlay">
			<div class="overlay-panel overlay-left">
				<h1>Welcome Back!</h1>
				<button class="ghost" id="signIn">Login</button>
			</div>
			<div class="overlay-panel overlay-right">
				<h1>Hello, Friend!</h1>
				<p>Enter your details and start journey with us!!</p>
				<button class="ghost" id="signUp">Sign Up</button>
			</div>
		</div>
	</div>
</div>
		</section>
<br> <br>
		
<script>
    const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');

signUpButton.addEventListener('click', () => {
	container.classList.add("right-panel-active");
});

signInButton.addEventListener('click', () => {
	container.classList.remove("right-panel-active");
});
</script>

    </body>

    </html>