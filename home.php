<?php 
 require_once 'config.php';
 session_start();
?>
<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device.width">
    <meta name="description" content="aquatic plants , IoT">
    <title>Green Help | Welcome</title>
    <link rel="stylesheet" href="./styles/home.css">
    <link rel="stylesheet" href="./styles/footer.css">
</head>

<body>
        <?php
        require_once 'header.php';
        ?>
      
        
    <div class="banner-img"></div>
      <div class="banner-text">
     
        <h2>Green-Help</h2>
        <p>"Feel free to keep up with your business throgh Green-Help!"</p>
           <a href="login.php" class="button">Log in / Signup</a>
      </div>
    </body>

    </html>
