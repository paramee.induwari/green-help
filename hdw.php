<?php 
 require_once 'config.php';
 session_start();
?>
<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device.width">
    <meta name="description" content="aquatic plants , IoT">
    <title>Green Help | Welcome</title>
    <link rel="stylesheet" href="./styles/hdw.css">
    <link rel="stylesheet" href="./styles/header.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"/>
</head>

<body>
        <?php
        require_once 'header.php';
        ?>
      
      <?php
      if(!isset($_SESSION['logedin'])){
          ?>
        <section id="cart">
        <div class="containerC">
                    
                     <div class="cart">
                            <div class="products">
                 <h3> You need to login first! </h3>
        </div>
    
        </div>
        </div>
        </section>
        <?php
    }
       
      else{
       $UserN = $_SESSION['UserN'];
       $Device = $_SESSION['device'];

       ?>
       <div class='grid-containerm'> 
             <h3> Device <?php echo $Device ; ?> is not in a stable environment! Please check the sensor log and make changes! </h3>
       </div>
       <?php

        $result = mysqli_query( $conn , "SELECT * FROM tempLog where deviceID = '$Device' ORDER BY dategh desc");
        if ($result !== false && $result ->num_rows > 0) {
          while($row = $result -> fetch_array())
         {
             $temp = $row['tempvalue'];
             $status = $row['tempstatus'];
             $dategh = $row['dategh'];
             ?>

             <div class='grid-containertemp'> 
                <div class='grid-item'> 
                    <h4> Device : <?php echo $Device ; ?>  </h4>
                    <h4> Sensor : Temperature</h4>
                    <img src="images/temp.jpeg" width="250px" height="250px">
                </div>
                <div class='grid-item'> 
                <h4> Date/Time : <?php echo $dategh ; ?>  </h4>
                </div>
                <div class='grid-item'> 
                <h4> Temp level : <?php echo $temp ; ?> degrees (C) </h4>
                </div>
                <div class='grid-item'> 
                <h4> Status : <?php echo $status ; ?>  </h4>
                </div>
             </div>
        <?php
    }
        }
        $result = mysqli_query( $conn , "SELECT * FROM wlLog where deviceID = '$Device' ORDER BY dategh desc");
        if ($result !== false && $result ->num_rows > 0) {
          while($row = $result -> fetch_array())
         {
             $wl = $row['wlvalue'];
             $status = $row['wlstatus'];
             $dategh = $row['dategh'];

        ?>
       <div class='grid-containerwl'> 
                <div class='grid-item'> 
                    <h4> Device : <?php echo $Device ; ?>  </h4>
                    <h4> Sensor : Ultrasonic (water level)</h4>
                    <img src="images/Ultra.jpg" width="250px" height="250px">
                </div>
                <div class='grid-item'> 
                <h4> Date/Time : <?php echo $dategh ; ?>  </h4>
                </div>
                <div class='grid-item'> 
                <h4> Water level : <?php echo $wl ; ?> mm </h4>
                </div>
                <div class='grid-item'> 
                <h4> Status : <?php echo $status ; ?>  </h4>
                </div>
             </div>
        <?php
    }
        }
        $result = mysqli_query( $conn , "SELECT * FROM phLog where deviceID = '$Device' ORDER BY dategh desc");
        if ($result !== false && $result ->num_rows > 0) {
          while($row = $result -> fetch_array())
         {
             $ph = $row['phvalue'];
             $status = $row['phstatus'];
             $dategh = $row['dategh'];

        ?>
          <div class='grid-containerph'> 
                <div class='grid-item'> 
                    <h4> Device : <?php echo $Device ; ?>  </h4>
                    <h4> Sensor : pH level</h4>
                    <img src="images/ph.jpg" width="250px" height="250px">
                </div>
                <div class='grid-item'> 
                <h4> Date/Time : <?php echo $dategh ; ?>  </h4>
                </div>
                <div class='grid-item'> 
                <h4> pH level : <?php echo $ph ; ?> </h4>
                </div>
                <div class='grid-item'> 
                <h4> Status : <?php echo $status ; ?>  </h4>
                </div>
             </div>
        
    <?php }
        }
    }
    
    ?>
    
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.7/dist/umd/popper.min.js" integrity="sha384-zYPOMqeu1DAVkHiLqWBUTcbYfZ8osu1Nd6Z89ify25QV9guujx43ITvfi12/QExE" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.min.js" integrity="sha384-Y4oOpwW3duJdCWv5ly8SCFYWqFDsfob/3GkgExXKV4idmbt98QcxXYs9UoXAB7BZ" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>   
    </body>

    </html>
