<?php 
 require_once 'config.php';
 session_start();
?>
<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device.width">
    <meta name="description" content="Garbage optimizing process | Colombo Municiple Council">
    <title>Green Help | Welcome</title>
    <link rel="stylesheet" href="./styles/home.css">
    <link rel="stylesheet" href="./styles/footer.css">
</head>

<body>
<?php
        require_once 'header.php';
        ?>

<?php
      $Uname = $_REQUEST["uname"];
      $Upass = $_REQUEST["pw"];
      $sql = " SELECT * FROM greenUser where Uname = '$Uname'";
      $result = mysqli_query( $conn , $sql);
      if (mysqli_num_rows($result)> 0) {
          $row = mysqli_fetch_assoc($result);
          if($row['Upass'] == $Upass){
                $_SESSION['logedin'] = true; 
                $_SESSION['UserN'] = $Uname;
                $device = $row['Udevice'];
                $_SESSION['device'] = $device;
                ?>
                <br> <br>
            <section id='show'>
              <div class='box'>
              <div class='inner-box'>
              <h4> successfully loged in </h4> 
              <h4> welcome back! </h4><br>
              <p style='text-align:right;'>
                    <a class='link' href='logout.php'>Log Out</a><br>
                    <a class='link' href='home.php'>Go to Home</a>

                </p>

              </div> </div> </section> 
              <?php 
            

          }
          else{
              ?>
              <br> <br>
            <section id='show'>
            <div class='box'>
            <div class='inner-box'>
            <h4> Oops! your password is incorrect </h4>
            <h4> Please enter the correct password </h4><br>
            <p style='text-align:right;'>
                  <a class='link' href='login.php'>Back to login</a><br>
                  <a class='link' href='home.php'>Go to Home</a>

              </p>
            </div> </div> </section>
            <?php
              
          }
      }
      else{
          ?>
          <br> <br>
        <section id='show'>
        <div class='box'>
        <div class='inner-box'>
        <h4> Oops! something went wrong </h4> 
        <h4> Please enter the correct username and try again! </h4>
        <br>
        <p style='text-align:right;'>
        <a class='link' href='login.php'>Back to login</a><br>
        <a class='link' href='home.php'>Go to Home</a>

          </p>
        </div> </div> </section>
        <?php
      }
    ?>

    </body>

    </html>